# Configuration Management Project

This project aims to automate the installation and configuration of SSH and Apache services using Puppet and Ansible. The project is structured to allow easy management and deployment of these services across different environments.

## Project Structure

- `puppet/`: Contains Puppet manifests and modules.
  - `manifests/`: Contains the main manifests for managing configurations.
    - `init.pp`: Manages dependencies between classes.
    - `params.pp`: Defines parameters for package installation.
    - `install.pp`: Handles the installation of required packages.
    - `config.pp`: Manages configuration files.
    - `service.pp`: Manages the services.
  - `templates/`: Contains template files for configurations.
    - `sshd_config.erb`: Template for SSH configuration.
    - `httpd.conf.erb`: Template for Apache configuration.
  - `files/`: Contains static files used in configurations.
  
- `ansible/`: Contains Ansible playbooks and inventories.
  - `playbooks/`: Contains the playbooks for SSH and Apache.
    - `ssh.yml`: Playbook for SSH configuration.
    - `apache.yml`: Playbook for Apache configuration.
  - `inventory/`: Contains inventory files for different environments.
    - `development`: Inventory file for the development environment.
    - `production`: Inventory file for the production environment.

```
├── puppet
│   ├── manifests
│   │   └── node.pp
│   └── modules
│       ├── my_awesome_httpd
│       │   ├── file
│       │   │   └── custom_apache_config.conf
│       │   ├── manifests
│       │   │   ├── config.pp
│       │   │   ├── init.pp
│       │   │   ├── install.pp
│       │   │   ├── params.pp
│       │   │   └── service.pp
│       │   └── templates
│       │       └── httpd.conf.erb
│       └── my_awesome_sshd
│           ├── file
│           │   └── custom_motd.txt
│           ├── manifests
│           │   ├── config.pp
│           │   ├── init.pp
│           │   ├── install.pp
│           │   ├── params.pp
│           │   └── service.pp
│           └── templates
│               └── sshd_config.erb
└── README.md
```

```
├── ansible
    ├── apache.yaml
    └── ssh.yaml
```

## Branches

- `main`: Main branch containing the final code.
- `development_puppet`: Development branch for Puppet manifests.
- `production_puppet`: Production branch for Puppet manifests.
- `test_puppet`: Test branch for Puppet manifests.
- `development_ansible`: Development branch for Ansible playbooks.
- `production_ansible`: Production branch for Ansible playbooks.

## Prerequisites

- **Git**: Ensure Git is installed on your system.
- **Puppet**: Ensure Puppet is installed and configured.
- **Ansible**: Ensure Ansible is installed.

## Setup Instructions

### Cloning the Repository

Clone the repository to your local machine:

\`\`\`sh
git clone https://forge.univ-lyon1.fr/p2313767/puppet
cd my_project
\`\`\`

### Branch Management

Switch to the desired branch:

\`\`\`sh
# Switch to the main branch
git checkout main

# Switch to the development branch for Puppet
git checkout development_puppet

# Switch to the development branch for Ansible
git checkout development_ansible
\`\`\`

## Usage

### Puppet

#### Applying Manifests

To apply the SSH manifest:

\`\`\`sh
sudo puppet apply --modulepath=$(pwd)/puppet -e "include my_awesome_sshd"
\`\`\`

To apply the Apache manifest:

\`\`\`sh
sudo puppet apply --modulepath=$(pwd)/puppet -e "include my_awesome_apache"
\`\`\`

### Ansible

#### Running Playbooks

To run the SSH playbook:

\`\`\`sh
ansible-playbook -i ansible/inventory/development ansible/playbooks/ssh.yml
\`\`\`

To run the Apache playbook:

\`\`\`sh
ansible-playbook -i ansible/inventory/development ansible/playbooks/apache.yml
\`\`\`

## Comparison of Puppet and Ansible

### Ease of Learning and Use
Puppet uses a declarative language specific to the tool, which can require some initial learning time. Ansible, on the other hand, uses YAML for its playbooks, which is easier to read and write for beginners.

### Configuration Management
Puppet uses Hiera for managing configurations and data, allowing a clear separation between code and data. Ansible uses inventory files and group variables, providing similar flexibility.

### Flexibility and Modularity
Puppet is highly modular and allows for easy creation of reusable modules. Ansible also offers great flexibility with its roles and collections.

### Specific Use Cases and Performance
Puppet is well-suited for environments where continuous configuration management is essential. Ansible is ideal for orchestration tasks and ad-hoc deployments due to its agentless nature.



## Authors

- PERRUCHE Arthur
- DA CRUZ Matéo


## License

This project is distributed under the **WTFPL 2.0**, which means you can copy, modify, distribute it under the same terms, etc. For more details, see the "LICENSE.pdf" file at the root of this project.