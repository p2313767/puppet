#node 'my_awesome_computer' {
  # Configuration pour my_awesome_sshd
 # $sshd_port = $facts['is_virtual'] ? {
  #  true    => 24,
   # default => 23,
  #}
  #$sshd_protocol = 2

  #class { 'my_awesome_sshd':
   # sshd_port     => $sshd_port,
   # sshd_protocol => $sshd_protocol,
  #}

  # Configuration pour my_awesome_httpd
  #$httpd_port = 80

  #class { 'my_awesome_httpd':
  #  httpd_port => $httpd_port,
  #}
#}
node default {
  include my_awesome_sshd
  include my_awesome_httpd
}
