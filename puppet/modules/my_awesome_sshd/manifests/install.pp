class my_awesome_sshd::install {
  package { 'openssh-server':
    ensure => installed,
  }
}
