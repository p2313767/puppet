class my_awesome_sshd::config {

$sshd_protocol = 2
  if $facts['virtual'] {
    $sshd_port = 24
  } else {
    $sshd_port = 23
  }

  file { '/etc/ssh/sshd_config':
    content => template('my_awesome_sshd/sshd_config.erb'),
    require => Package['openssh-server'],
    notify  => Service['sshd'],
  }
}
