class my_awesome_sshd::service {
  service { 'sshd':
    ensure => running,
    enable => true,
  }
}
