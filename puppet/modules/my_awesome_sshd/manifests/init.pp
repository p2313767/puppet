# my_awesome_sshd::init
#
# This class manages the initialization of the my_awesome_sshd module.
#
class my_awesome_sshd {
  Class['my_awesome_sshd::params']
  -> Class['my_awesome_sshd::install']
  -> Class['my_awesome_sshd::config']
  -> Class['my_awesome_sshd::service']

  include my_awesome_sshd::params
  include my_awesome_sshd::install
  include my_awesome_sshd::config
  include my_awesome_sshd::service
}
