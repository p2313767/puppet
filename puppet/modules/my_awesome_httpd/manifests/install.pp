class my_awesome_httpd::install {
  package { 'apache2':
    ensure => installed,
  }
}
