# my_awesome_httpd::init
#
# This class manages the initialization of the my_awesome_httpd module.
#
class my_awesome_httpd {
  Class['my_awesome_httpd::params']
  -> Class['my_awesome_httpd::install']
  -> Class['my_awesome_httpd::config']
  -> Class['my_awesome_httpd::service']

  include my_awesome_httpd::params
  include my_awesome_httpd::install
  include my_awesome_httpd::config
  include my_awesome_httpd::service
}
