class my_awesome_httpd::service {
  service { 'apache2':
    ensure => running,
    enable => true,
  }
}
