class my_awesome_httpd::config {
  $server_name = $my_awesome_httpd::params::server_name

  file { '/etc/apache2/sites-enabled/000-default.conf':
    content => template('my_awesome_httpd/httpd.conf.erb'),
    require => Package['apache2'],
    notify  => Service['apache2'],
  }
}
